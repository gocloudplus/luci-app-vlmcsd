module("luci.controller.vlmcsd", package.seeall)

function index()
	if not nixio.fs.access("/etc/config/vlmcsd") then
		return
	end

	page = entry({"admin", "intf_advanced", "vlmcsd"}, cbi("vlmcsd"), _("vlmcsd"), 100)
	page.i18n = "vlmcsd"
	page.dependent = true
end
